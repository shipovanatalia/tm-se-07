package ru.shipova.tm;

import ru.shipova.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
