package ru.shipova.tm.bootstrap;

import ru.shipova.tm.api.ServiceLocator;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.command.project.ProjectClearCommand;
import ru.shipova.tm.command.project.ProjectCreateCommand;
import ru.shipova.tm.command.project.ProjectListCommand;
import ru.shipova.tm.command.project.ProjectRemoveCommand;
import ru.shipova.tm.command.system.AboutCommand;
import ru.shipova.tm.command.system.ExitCommand;
import ru.shipova.tm.command.system.HelpCommand;
import ru.shipova.tm.command.task.*;
import ru.shipova.tm.command.user.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.TerminalService;
import ru.shipova.tm.service.UserService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс загрузчика приложения
 */

public final class Bootstrap implements ServiceLocator {

    private final ITaskRepository iTaskRepository = new TaskRepository();
    private final IProjectRepository iProjectRepository = new ProjectRepository();
    private final IProjectService iProjectService = new ProjectService(iProjectRepository, iTaskRepository);
    private final ITaskService iTaskService = new TaskService(iTaskRepository, iProjectRepository);
    private final TerminalService terminalService = new TerminalService(this);
    private final IUserRepository iUserRepository = new UserRepository();
    private final IUserService userService = new UserService(iUserRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String commandName = command.getName(); //Command Line Interface
        final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    private void execute(final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        final User currentUser = userService.getCurrentUser();


        if (currentUser == null) {
            if (abstractCommand.needAuthorize()) {
                System.out.println("ACCESS ERROR");
                return;
            } else {
                abstractCommand.execute();
            }
            return;
        }

        final String role = currentUser.getRoleType().toString();

        if (role == null || role.isEmpty()) return;

        if (RoleType.USER.name().equals(role)) {
            if (abstractCommand.isOnlyAdminCommand()) {
                System.out.println("ACCESS ERROR");
                return;
            } else abstractCommand.execute();
        }

        if (RoleType.ADMIN.name().equals(role)) {
            abstractCommand.execute();
        }

    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws Exception {
        loadCommands();

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            execute(commandName);
        }
    }

    private void loadCommands() {
        try {
            registry(new HelpCommand());
            registry(new ExitCommand());
            registry(new ProjectListCommand());
            registry(new ProjectCreateCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectRemoveCommand());
            registry(new TaskClearCommand());
            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskRemoveCommand());
            registry(new TaskShowCommand());
            registry(new UserLoginCommand());
            registry(new UserLogoutCommand());
            registry(new UserRegistryCommand());
            registry(new UserSetPasswordCommand());
            registry(new UserProfileCommand());
            registry(new UserUpdateCommand());
            registry(new AboutCommand());
        } catch (CommandCorruptException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public IProjectService getIProjectService() {
        return iProjectService;
    }

    @Override
    public ITaskService getITaskService() {
        return iTaskService;
    }

    @Override
    public IUserService getIUserService() {
        return userService;
    }
}
