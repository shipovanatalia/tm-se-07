package ru.shipova.tm.entity;

import ru.shipova.tm.constant.RoleType;

public final class User extends AbstractEntity{
    private String id;
    private String login;
    private RoleType roleType;
    private String passwordHash;

    public User(final String id, final String login, final String passwordHash, final RoleType roleType) {
        this.id = id;
        this.login = login;
        this.roleType = roleType;
        this.passwordHash = passwordHash;
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public void setRoleType(final RoleType roleType) {
        this.roleType = roleType;
    }

    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
