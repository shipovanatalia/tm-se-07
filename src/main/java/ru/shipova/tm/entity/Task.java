package ru.shipova.tm.entity;

import java.util.Date;

public final class Task extends AbstractEntity {
    private String id;
    private String projectId;
    private String name;
    private String description;
    private Date dateOfBegin;
    private Date dateOfEnd;
    private String userId;

    public Task(final String id, final String name, final String projectId, final String userId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateOfBegin() {
        return dateOfBegin;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public String getUserId() {
        return userId;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setDateOfBegin(final Date dateOfBegin) {
        this.dateOfBegin = dateOfBegin;
    }

    public void setDateOfEnd(final Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }
}
