package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Sign out from Task Manager.";
    }

    @Override
    public void execute(){
        final IUserService iUserService = serviceLocator.getIUserService();
        iUserService.setCurrentUser(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
