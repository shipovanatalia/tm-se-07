package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update role type of user.";
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE]");
        final IUserService iUserService = serviceLocator.getIUserService();

        System.out.println("ENTER LOGIN OF USER TO UPDATE:");
        final String login = serviceLocator.getTerminalService().nextLine();

        System.out.println("ENTER NEW ROLE TYPE:");
        final String newRoleType = serviceLocator.getTerminalService().nextLine();
        iUserService.updateUser(login, newRoleType);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
