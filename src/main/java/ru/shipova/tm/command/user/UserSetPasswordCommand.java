package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserSetPasswordCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-set-password";
    }

    @Override
    public String getDescription() {
        return "Set new password.";
    }

    @Override
    public void execute() {
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        String userToUpdate = currentUser.getLogin();

        if ("ADMIN".equals(currentUser.getRoleType().toString())){
            System.out.println("ENTER LOGIN OF USER TO UPDATE:");
            userToUpdate = serviceLocator.getTerminalService().nextLine();
        }

        System.out.println("ENTER NEW PASSWORD");
        final String password1 = serviceLocator.getTerminalService().nextLine();
        System.out.println("REPEAT NEW PASSWORD");
        final String password2 = serviceLocator.getTerminalService().nextLine();
        if (password1.equals(password2)) {
            iUserService.setNewPassword(userToUpdate, password1);
            System.out.println("[OK]");
        } else {
            System.out.println("PASSWORDS DO NOT MATCH.");
            execute();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
