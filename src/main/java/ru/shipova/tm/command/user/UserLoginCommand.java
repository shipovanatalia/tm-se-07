package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserLoginCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = serviceLocator.getTerminalService().nextLine();
        final IUserService iUserService = serviceLocator.getIUserService();
        final User user = iUserService.authorize(login, password);
        if (user == null) {
            System.out.println("WRONG LOGIN OR PASSWORD");
            execute();
        }
        iUserService.setCurrentUser(user);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
