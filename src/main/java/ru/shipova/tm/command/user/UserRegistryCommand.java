package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;

public final class UserRegistryCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public String getDescription() {
        return "Registry new user.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.println("ENTER LOGIN");
        final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER ROLE OF USER");
        final String roleType = serviceLocator.getTerminalService().nextLine();
        final IUserService iUserService = serviceLocator.getIUserService();
        iUserService.registryUser(login, password, roleType);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
