package ru.shipova.tm.command.user;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Get all information about user.";
    }

    @Override
    public void execute() {
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        if (currentUser == null) {
            System.out.println("PLEASE LOGIN TO GET INFORMATION");
            return;
        }
        System.out.println("[USER PROFILE]");
        System.out.println("USER LOGIN: " + currentUser.getLogin());
        System.out.println("USER ROLE TYPE: " + currentUser.getRoleType().displayName());
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
