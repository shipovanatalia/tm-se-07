package ru.shipova.tm.command.task;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

public final class TaskShowCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-show";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF PROJECT:");
        final ITaskService iTaskService = serviceLocator.getITaskService();
        final String projectName = serviceLocator.getTerminalService().nextLine();
        System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");
        try {
            for (String nameOfTask : iTaskService.showAllTasksOfProject(projectName)) {
            System.out.println(nameOfTask);}
        } catch (ProjectDoesNotExistException e) {
            System.out.println("PROJECT DOES NOT EXISTS");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
