package ru.shipova.tm.command.task;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        final ITaskService iTaskService = serviceLocator.getITaskService();
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();
        iTaskService.clear(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

}
