package ru.shipova.tm.command.task;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final ITaskService iTaskService = serviceLocator.getITaskService();
        final String taskName = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = serviceLocator.getTerminalService().nextLine();

        final String userId = serviceLocator.getIUserService().getCurrentUser().getId();

        try {
            iTaskService.create(userId, taskName, projectName);
        } catch (ProjectDoesNotExistException e) {
            System.out.println("PROJECT DOES NOT EXISTS");
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
