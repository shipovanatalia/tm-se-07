package ru.shipova.tm.command.task;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF TASK:");
        final ITaskService iTaskService = serviceLocator.getITaskService();
        final String projectName = serviceLocator.getTerminalService().nextLine();

        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();

        iTaskService.remove(userId, projectName);
        System.out.println("[TASK " + projectName + " REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
