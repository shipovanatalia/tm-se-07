package ru.shipova.tm.command.task;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;

public final class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        final ITaskService iTaskService = serviceLocator.getITaskService();
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();

        int index = 1;
        for (Task task : iTaskService.getListTask(userId)) {
            System.out.println(index++ + ". " + task.getName());
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
