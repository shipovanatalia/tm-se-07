package ru.shipova.tm.command.system;

import ru.shipova.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute(){
        System.exit(0);
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
