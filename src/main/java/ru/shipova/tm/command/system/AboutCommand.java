package ru.shipova.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.shipova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Display information about application.";
    }

    @Override
    public void execute() {
        System.out.println("Manifest Version is " + Manifests.read("Manifest-Version"));
        System.out.println("Сurrent build number is " + Manifests.read("AppVersion"));
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
