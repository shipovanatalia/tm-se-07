package ru.shipova.tm.command.project;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.command.AbstractCommand;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final IProjectService iProjectService = serviceLocator.getIProjectService();
        final String projectName = serviceLocator.getTerminalService().nextLine();
        final String userId = serviceLocator.getIUserService().getCurrentUser().getId();
        iProjectService.create(userId, projectName);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }


}
