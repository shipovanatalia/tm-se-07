package ru.shipova.tm.command.project;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final IProjectService iProjectService = serviceLocator.getIProjectService();
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();
        iProjectService.clear(userId);
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
