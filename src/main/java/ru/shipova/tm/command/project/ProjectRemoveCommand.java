package ru.shipova.tm.command.project;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF PROJECT:");
        final IProjectService iProjectService = serviceLocator.getIProjectService();
        final String projectName = serviceLocator.getTerminalService().nextLine();

        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();

        iProjectService.remove(userId, projectName);
        System.out.println("[PROJECT " + projectName + " REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
