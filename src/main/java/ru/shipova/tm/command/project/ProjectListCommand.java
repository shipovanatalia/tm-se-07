package ru.shipova.tm.command.project;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        final IProjectService iProjectService = serviceLocator.getIProjectService();
        final IUserService iUserService = serviceLocator.getIUserService();
        final User currentUser = iUserService.getCurrentUser();
        final String userId = currentUser.getId();

        int index = 1;
        for (Project project : iProjectService.getListProject(userId)) {
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
