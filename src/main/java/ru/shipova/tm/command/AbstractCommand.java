package ru.shipova.tm.command;

import ru.shipova.tm.api.ServiceLocator;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute() throws Exception;
    public abstract boolean needAuthorize();
    public boolean isOnlyAdminCommand(){
        return false;
    }
}
