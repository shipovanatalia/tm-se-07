package ru.shipova.tm.repository;

import ru.shipova.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> {
    private final Map<String, T> map = new HashMap<>();

    public void persist(final T t) {
        if (isExist(t)) return;
        map.put(t.getId(), t);
    }

    public boolean isExist(final T t) {
        String id = t.getId();
        return map.containsKey(id);
    }

    public List<T> findAll() {
        return new ArrayList<>(map.values());
    }

    public T findOne(final String id) {
        return map.get(id);
    }

    public void remove(final String id) {
        map.remove(id);
    }

    public void remove(final T t) {
        final String id = t.getId();
        map.remove(id);
    }

    public void merge(final T t) {
        map.put(t.getId(), t);
    }

    public Map<String, T> getMap() {
        return map;
    }
}
