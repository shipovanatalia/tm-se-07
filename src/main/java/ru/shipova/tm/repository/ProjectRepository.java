package ru.shipova.tm.repository;

import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.entity.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAllByUserId(final String userId) {
        final List<Project> projects = new ArrayList<>();

        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())) {
                projects.add(entry.getValue());
            }
        }
        return projects;
    }

    @Override
    public String getProjectIdByName(final String projectName) {
        final Iterator<Map.Entry<String, Project>> iterator = getMap().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public void removeAllByUserId(final String userId) {
        final List<Project> projectsToRemove = findAllByUserId(userId);
        for (Project project : projectsToRemove) {
            for (Map.Entry<String, Project> entry : getMap().entrySet()) {
                if (project.getId().equals(entry.getKey())) {
                    remove(project);
                }
            }
        }
    }

    @Override
    public void update(final Project project) {
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
                entry.getValue().setDateOfBegin(project.getDateOfBegin());
                entry.getValue().setDateOfEnd(project.getDateOfEnd());
            }
        }
    }
}
