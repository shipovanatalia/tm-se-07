package ru.shipova.tm.repository;

import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.Map;
import java.util.UUID;

import static ru.shipova.tm.constant.RoleType.ADMIN;
import static ru.shipova.tm.constant.RoleType.USER;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
        final String userId = UUID.randomUUID().toString();
        final String userLogin = "user";
        final String userPasswordHash = PasswordHashUtil.md5("user");
        getMap().put(userId, new User (userId,userLogin, userPasswordHash, USER));


        final String adminId = UUID.randomUUID().toString();
        final String adminLogin = "admin";
        final String adminPasswordHash = PasswordHashUtil.md5("admin");
        getMap().put(adminId, new User(adminId, adminLogin, adminPasswordHash, ADMIN));
    }

    @Override
    public User findByLogin(final String login) {
        String userId = "";
        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                userId = entry.getKey();
            }
        }
        if (!getMap().containsKey(userId)) return null;
        return getMap().get(userId);
    }

    @Override
    public void setNewPassword(final String login, final String passwordHash) {
        final User user = findByLogin(login);
        if (!isExist(user)) return;

        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (user.getId().equals(entry.getKey())) {
                entry.getValue().setPasswordHash(passwordHash);
            }
        }
    }
    
    @Override
    public void updateUser(final String login, final RoleType roleType){
        final User user = findByLogin(login);
        if (!isExist(user)) return;
        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (user.getId().equals(entry.getKey())) {
                entry.getValue().setRoleType(roleType);
            }
        }
    }
}
