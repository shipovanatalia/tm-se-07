package ru.shipova.tm.api.service;

import ru.shipova.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    void create(final String userId, final String projectName);

    List<Project> getListProject(final String userId);

    void clear(final String userId);

    void remove(final String userId, final String projectName);
}
