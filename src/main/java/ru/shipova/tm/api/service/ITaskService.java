package ru.shipova.tm.api.service;

import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

import java.util.List;

public interface ITaskService {
    void create(final String userId, final String taskName, final String projectName) throws ProjectDoesNotExistException;

    List<String> showAllTasksOfProject(final String projectName) throws ProjectDoesNotExistException;

    List<Task> getListTask(final String userId);

    void clear(final String userId);

    void remove(final String userId, final String taskName);
}
