package ru.shipova.tm.api.service;

import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;

public interface IUserService {
    void registryUser(final String login, final String password, final String role);

    void updateUser(final String login, final String role);

    RoleType getRoleType(final String role);

    void setNewPassword(final String userLogin, final String password);

    User authorize(final String login, final String password);

    boolean checkDataAccess(final String login, final String password);

    User findByLogin(final String login);

    void setCurrentUser(final User currentUser);

    User getCurrentUser();
}
