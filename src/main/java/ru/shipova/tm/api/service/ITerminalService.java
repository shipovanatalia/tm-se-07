package ru.shipova.tm.api.service;

import ru.shipova.tm.command.AbstractCommand;

import java.util.List;

public interface ITerminalService {
    List<AbstractCommand> getCommands();

    String nextLine();
}
