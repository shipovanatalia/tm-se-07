package ru.shipova.tm.api;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.service.TerminalService;

public interface ServiceLocator {
    TerminalService getTerminalService();
    IProjectService getIProjectService();
    ITaskService getITaskService();
    IUserService getIUserService();
}
