package ru.shipova.tm.api.repository;

import ru.shipova.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
     Project findOne(final String projectId);

     List<Project> findAll();

     List<Project> findAllByUserId(final String userId);

     String getProjectIdByName(final String projectName);

     void persist(final Project project);

     void merge(final Project project);

     void remove(final String projectId);

     void remove(final Project project);

     void removeAllByUserId(final String userId);

     void update(final Project project);
}
