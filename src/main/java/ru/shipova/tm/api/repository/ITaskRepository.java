package ru.shipova.tm.api.repository;

import ru.shipova.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    Task findOne(final String taskId);

    List<Task> findAll();

    List<Task> findAllByUserId(final String userId);

    String getTaskIdByName(final String taskName);

    /**
     * Метод вставляет новый объект, если его не было. Данные объекта не обновляет и не перезатирает.
     *
     * @param task - объект, который необходимо вставить
     */

    void persist(final Task task);

    /**
     * Метод вставляет новый объект, если его не было.
     * Если объект был, он его обновляет.
     *
     * @param task - объект, который необходимо вставить
     */
    void merge(final Task task);

    void remove(final String taskId);

    void remove(final Task task);

    void removeAllByUserId(final String userId);

    /**
     * Метод находит объект с идентичным id и обновляет все его поля на поля объекта task.
     *
     * @param task - объект с новыми данными.
     */
    void update(final Task task);

    List<String> showAllTasksOfProject(final String projectId);

    void removeAllTasksOfProject(final String projectId);
}
