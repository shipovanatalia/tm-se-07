package ru.shipova.tm.api.repository;

import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;

public interface IUserRepository {
    User findByLogin(final String login);

    void persist(final User user);

    void setNewPassword(final String login, final String passwordHash);

    void updateUser(final String login, final RoleType roleType);
}
