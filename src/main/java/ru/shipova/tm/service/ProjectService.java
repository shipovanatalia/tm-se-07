package ru.shipova.tm.service;

import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;

import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    private final IProjectRepository iProjectRepository;
    private final ITaskRepository iTaskRepository;

    public ProjectService(final IProjectRepository iProjectRepository, final ITaskRepository iTaskRepository) {
        this.iProjectRepository = iProjectRepository;
        this.iTaskRepository = iTaskRepository;
    }

    @Override
    public void create(final String userId, final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        final String projectId = UUID.randomUUID().toString();
        iProjectRepository.persist(new Project(projectId, projectName, userId));
    }

    @Override
    public List<Project> getListProject(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return iProjectRepository.findAllByUserId(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        iProjectRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(final String userId, final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        final String projectId = iProjectRepository.getProjectIdByName(projectName);

        final Project project = iProjectRepository.findOne(projectId);
        if (project == null) return;
        if (!userId.equals(project.getUserId())) return;

        iProjectRepository.remove(projectId);
        iTaskRepository.removeAllTasksOfProject(projectId);
    }
}
