package ru.shipova.tm.service;

import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;

import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    private final  ITaskRepository iTaskRepository;
    private final  IProjectRepository iProjectRepository;

    public TaskService(final ITaskRepository iTaskRepository, final IProjectRepository iProjectRepository) {
        this.iTaskRepository = iTaskRepository;
        this.iProjectRepository = iProjectRepository;
    }

    @Override
    public void create(final String userId, final String taskName, final String projectName) throws ProjectDoesNotExistException {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty()) return;

        final String projectId = iProjectRepository.getProjectIdByName(projectName);
        if (iProjectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        final String taskId = UUID.randomUUID().toString();
        iTaskRepository.persist(new Task(taskId, taskName, projectId, userId));
    }

    @Override
    public List<String> showAllTasksOfProject(final String projectName) throws ProjectDoesNotExistException {
        if (projectName == null || projectName.isEmpty()) return null;

        final String projectId = iProjectRepository.getProjectIdByName(projectName);
        if (iProjectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        return iTaskRepository.showAllTasksOfProject(projectId);
    }

    @Override
    public List<Task> getListTask(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return iTaskRepository.findAllByUserId(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        iTaskRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(final String userId, final String taskName) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        final String taskId = iTaskRepository.getTaskIdByName(taskName);
        final Task task = iTaskRepository.findOne(taskId);

        if (task == null) return;
        if (!userId.equals(task.getUserId())) return;

        iTaskRepository.remove(taskId);
    }
}
