package ru.shipova.tm.service;

import ru.shipova.tm.api.service.ITerminalService;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.command.AbstractCommand;

import java.util.List;
import java.util.Scanner;

public final class TerminalService implements ITerminalService {
    private final Bootstrap bootstrap;
    private final Scanner in = new Scanner(System.in);

    public TerminalService(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public List<AbstractCommand> getCommands(){
        return bootstrap.getCommands();
    }

    @Override
    public String nextLine(){
        return in.nextLine();
    }
}
