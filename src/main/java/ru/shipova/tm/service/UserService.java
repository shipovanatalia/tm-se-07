package ru.shipova.tm.service;

import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.UUID;

public final class UserService implements IUserService {

    private final IUserRepository iUserRepository;
    private User currentUser;

    public UserService(final IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
        currentUser = null;
    }

    @Override
    public void registryUser(final String login, final String password, final String role) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;

        final String passwordHash = PasswordHashUtil.md5(password);
        final String userId = UUID.randomUUID().toString();

        final RoleType roleType = getRoleType(role);
        iUserRepository.persist(new User(userId, login, passwordHash, roleType));
    }

    @Override
    public void updateUser(final String login, final String role){
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        final RoleType roleType = getRoleType(role);
        iUserRepository.updateUser(login, roleType);
    }

    @Override
    public RoleType getRoleType(final String role){
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    @Override
    public void setNewPassword(final String login, final String password) {
        if (password == null || password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        final String passwordHash = PasswordHashUtil.md5(password);
        iUserRepository.setNewPassword(login, passwordHash);
    }

    @Override
    public User authorize(final String login, final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        return iUserRepository.findByLogin(login);
    }

    @Override
    public boolean checkDataAccess(final String login, final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = findByLogin(login);
        if (user == null) return false;
        final String passwordHash = PasswordHashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return iUserRepository.findByLogin(login);
    }

    @Override
    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }
}
